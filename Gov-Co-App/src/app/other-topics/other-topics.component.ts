import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-other-topics',
  templateUrl: './other-topics.component.html',
  styleUrls: ['./other-topics.component.css']
})
export class OtherTopicsComponent implements OnInit {

  
  Data: any;

  constructor( private FireDB : AngularFireDatabase) { }

  ngOnInit(): void {
    this.Data = this.FireDB.object("topic").valueChanges();
    this.Data.subscribe({
      next: this.onDataLoaded.bind(this)
      
    });
  }

  onDataLoaded(data) {
    
    this.Data = data.topics;
    
  }

  
}

