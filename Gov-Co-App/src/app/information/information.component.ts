import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {
 
  Cards: any;
  Dataloaded: boolean = false;
 imgcar1:string = "url('../../../assets/images/2.png')";
 imgcar2:string = "url('../../../assets/images/3.png')";
 imgcar3:string = "url('../../../assets/images/4.png')";
 imgcar4:string = "url('../../../assets/images/5.png')";

  constructor(private firebaseDatabase : AngularFireDatabase) { }

  ngOnInit(): void {
    this.Cards = this.firebaseDatabase.object("cards").valueChanges();
    this.Cards.subscribe({
      next: this.onDataLoaded.bind(this),
     
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Cards = data;
    console.log("cards", data);
  }

  
}
