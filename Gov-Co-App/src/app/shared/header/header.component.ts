import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {  InformationService } from '../../information.service';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  InfoService: Observable<any>
  
  Data: any

  constructor( private FireBD : AngularFireDatabase) { }

  btnSearchGovCo() { }

  changeLang() { }

  ngOnInit(): void {
    this.InfoService = this.FireBD.object("base").valueChanges();
    this.InfoService.subscribe({
      next: this.LoadData.bind(this)
     
    });
  }

  LoadData(data) {
   
    this.Data = data;
    console.log(data);
  }

 
}
