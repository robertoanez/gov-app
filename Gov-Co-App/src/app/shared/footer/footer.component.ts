import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  InfoService: Observable<any>
  Data: any 

  constructor( private FireDB : AngularFireDatabase) { }

  ngOnInit(): void {
    this.InfoService = this.FireDB.object("base").valueChanges();
    this.InfoService.subscribe({
      next: this.LoadData.bind(this),
      
    });
  }

  LoadData(data) {
   
    this.Data = data;
  }

  
}
