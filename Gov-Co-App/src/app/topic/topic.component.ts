import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {

 
  @Input()
  title: string = "title";

  @Input()
  image: string = "link";

  @Input()
  link: string = "link";

  @Input()
  description: string = "description"

  constructor() { }

  ngOnInit(): void {
  }

}
