import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-opinion',
  templateUrl: './opinion.component.html',
  styleUrls: ['./opinion.component.css']
})
export class OpinionComponent implements OnInit {

  Dataloaded = false;
  Tags: any = []
  opinions: any = []
  yourVariable: string = "jose";
  contador: number = 1;
  textareaValue: string = "";
  Opinions: Observable<any>;
  isFirstLoad: Boolean = true;

  constructor(private  FireDB: AngularFireDatabase) { }

  ngOnInit(): void {
    this.isFirstLoad = true;
    this.Tags = this.FireDB.object("opinion").valueChanges();
    this.Opinions = this.FireDB.object("user_opinion").valueChanges();

    this.Tags.subscribe({
      next: this.onDataLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });

    this.Opinions.subscribe({
      next: this.onDataOpinionLoaded.bind(this),
      error: this.onErrorDataLoaded.bind(this)
    });
  }

  onDataLoaded(data) {
    this.Dataloaded = true;
    this.Tags = data.tags;
    console.log(data);
  }

  onErrorDataLoaded(error) {
    console.log(error)
  }

  onDataOpinionLoaded(data) {
    this.opinions = data.opinions || [];
    let count = this.opinions.length;    

    console.log(count, this.contador, this.opinions, this.isFirstLoad);
    if (count > this.contador && !this.isFirstLoad) {
      alert(" hay registradas " + this.contador + " opiniones, su opinion ha sido guardada exitosamente");
    }

    if (this.isFirstLoad) {
      this.isFirstLoad = false;
    }
    this.contador = count;
    console.log(count, this.contador, this.opinions, this.isFirstLoad);
  }

  sendOpinion() {
    if (this.textareaValue.length > 0) {
      console.log("to save: ",);
      this.FireDB.object("user_opinion").set({ opinions: [...this.opinions, this.textareaValue] });
      this.textareaValue = ""
    } else {
      alert("debe escribir una opinion");
    }
  }
}
