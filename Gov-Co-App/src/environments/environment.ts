// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCHZHAZSnZK7bgrTcGIEybXPL6tdH4bD9I",
    authDomain: "gov-app-test.firebaseapp.com",
    databaseURL: "https://gov-app-test.firebaseio.com",
    projectId: "gov-app-test",
    storageBucket: "gov-app-test.appspot.com",
    messagingSenderId: "480195591952",
    appId: "1:480195591952:web:f500f16207a4819dfe6f95"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
